package no.ntnu.henriabu.mappe.del1.hospital;

import java.util.ArrayList;

/**
 * Class hospital represents a hospital storing the information from the departments in the hospital.
 */

public class Hospital {
    private String hospitalName;
    private ArrayList<Department> departments;

    public Hospital(String hospitalName){
        if (hospitalName == null || hospitalName.equals("")) {
            throw new IllegalArgumentException("Hospital must have a name");
        }
        this.hospitalName = hospitalName;
        this.departments = new ArrayList<>();
    }

    public String getHospitalName(){
        return hospitalName;
    }

    public ArrayList<Department> getDepartments(){
        return departments;
    }

    /**
     * Method checks if the department is alreadu in the list. If so an IllegalArgumentException is thrown.
     * If not department is added.
     * @param department
     */
    public void addDepartment(Department department){
        if(departments.contains(department)){
            throw new IllegalArgumentException("Department " + department + " is already in the list");
        }
        departments.add(department);
    }

    /**
     * toString uses Stringbuilder and the departments' toStrings to make a readable hospital overview
     * @return
     */
    public String toString(){
        StringBuilder text = new StringBuilder();
        text.append(hospitalName + ":\n\n");
        for (Department department: departments){
            text.append(department.toString());
        }
        return text.toString();
    }
}
