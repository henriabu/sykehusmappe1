package no.ntnu.henriabu.mappe.del1.hospital;

import no.ntnu.henriabu.mappe.del1.hospital.exception.RemoveException;

import java.util.HashMap;
import java.util.Objects;

/**
 * Class department represents one department in the hospital with the registers for employees and patients
 * connected to the department.
 * Since a person's social security number is unique - HashMap is used with the SSN as the key. This makes it
 * easy to loop through the lists looking for a particular person.
 */

public class Department {
    private String departmentName;
    private HashMap<String, Patient> patients;
    private HashMap<String, Employee> employees;

    /**
     *
     * @param departmentName
     */

    public Department(String departmentName){
        if (departmentName == null || departmentName.equals("")){
            throw new IllegalArgumentException("Department must have a name");
        }
        this.departmentName = departmentName;
        this.patients = new HashMap<>();
        this.employees = new HashMap<>();
    }

    public void setDepartmentName(String departmentName){
        this.departmentName = departmentName;
    }
    public String getDepartmentName(){
        return departmentName;
    }

    /**
     * Method addPatient adds patient to the patients list if there is no other patient with the same SSN
     * in the list.
     * @param patient
     */
    public void addPatient(Patient patient){
        if (patients.containsKey(patient.getSocialSecurityNumber())){
        throw new IllegalArgumentException("Patient " + patient.getFullName() + " is already registered");
        }
        patients.put(patient.getSocialSecurityNumber(), patient);
    }

    public HashMap<String, Patient> getPatients(){
        return patients;
    }

    /**
     * Method addEmployee adds employee to the employees list if there is no other employee with the same SSN
     * @param employee
     */
    public void addEmployee(Employee employee){
        if (employees.containsKey(employee.getSocialSecurityNumber())){
            throw new IllegalArgumentException("Employee " + employee.getFullName() + " is already registered");
        }
        employees.put(employee.getSocialSecurityNumber(), employee);
    }

    public HashMap<String, Employee> getEmployees(){
        return employees;
    }

    /**
     *
     * @param o An object we want to compare
     * @return Method equals returns true if departments has the same name. If not false is returned.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (! (o instanceof Department)){
            return false;
        }
        Department department = (Department) o;
        return department.getDepartmentName().equalsIgnoreCase(this.getDepartmentName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }

    /**
     * Checks whether person is an instance of Patient or Employee. If person does not already exist in
     * the list, RemoveException is thrown. If person is in the list the person is removed.
     * @param person
     * @throws RemoveException
     */
    public void remove(Person person) throws RemoveException {
        if (person instanceof Patient){
            Patient patient = (Patient) person;
            if (!patients.containsKey(patient.getSocialSecurityNumber())){
                throw new RemoveException(patient.getFullName() + " was not found in the register");
            }
            patients.remove(patient);
        }
        else if(person instanceof Employee){
            Employee employee = (Employee) person;
            if (!employees.containsKey(employee.getSocialSecurityNumber())){
                throw new RemoveException(employee.getFullName() + " was not found in the register");
            }
            employees.remove(employee);
        }
    }

    /**
     * ToString uses StringBuilder and the Objects' toStrings to make an easy to read overview of the department
     * @return text.toString()
     */
    public String toString(){
        StringBuilder text = new StringBuilder();
        text.append("Departement: " + departmentName);
        text.append("\n\nPatients:\n");
        for(Patient patient: patients.values()){
            text.append(" " + patient.toString());
        }
        text.append("\n\nEmployees: \n");
        for (Employee employee: employees.values()){
            text.append(" " + employee.toString());
        }
        text.append("\n\n");
        return text.toString();
    }

}
