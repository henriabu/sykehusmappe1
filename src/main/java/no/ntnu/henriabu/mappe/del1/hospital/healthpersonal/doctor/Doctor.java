package no.ntnu.henriabu.mappe.del1.hospital.healthpersonal.doctor;

import no.ntnu.henriabu.mappe.del1.hospital.Employee;
import no.ntnu.henriabu.mappe.del1.hospital.Patient;

/**
 * Class doctor is an abstract superclass for GeneralPractitioner and Surgeon.
 */
public abstract class Doctor extends Employee {
    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
