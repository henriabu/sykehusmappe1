package no.ntnu.henriabu.mappe.del1.hospital;


import no.ntnu.henriabu.mappe.del1.hospital.exception.RemoveException;

public class HospitalClient {
    public static void main(String[] args) throws RemoveException {
        Hospital hospital = new Hospital("NTNU-sjukehus");
        HospitalTestData.fillRegisterWithTestData(hospital);

        //Removes Odd Even Primtallet for department Emergency.
        hospital.getDepartments().get(0).remove(hospital.getDepartments().get(0).getEmployees().get(0));

        Employee employee1 = new Employee("Johannes", "Bø", "20");
        //Attempt to remove employee who is not in the register. Throws RemoveException.
        try {
            hospital.getDepartments().get(0).remove(employee1);
        }
        catch (RemoveException e){
            System.out.println(e.fillInStackTrace());
        }
    }
}
