package no.ntnu.henriabu.mappe.del1.hospital.healthpersonal;

import no.ntnu.henriabu.mappe.del1.hospital.Employee;

public class Nurse extends Employee {
    public Nurse(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }
    /**
     *
     * @return Does not return social security number since the toString will be used in the departments
     * toString and the SSN is sensitive information.
     */
    public String toString(){
        return "Nurse." + " Name: " + getFullName() + "\n";
    }
}
