package no.ntnu.henriabu.mappe.del1.hospital;

/**
 * Abstract super class for all persons.
 */
public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * Throws exception if either first or last name is not filled out.
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber Should really be eleven digits long, but for the sake of simplicity
     *                             we ignore that here.
     */

    public Person(String firstName, String lastName, String socialSecurityNumber){
        if (firstName.equals("") || firstName == null){
            throw new IllegalArgumentException("First name must be filled out.");
        }
        this.firstName = firstName;
        if (lastName.equals("") || lastName == null){
            throw new IllegalArgumentException("Last name must be filled out.");
        }
        this.lastName = lastName;
        //If the program was to demand an a proper SSN with 11 digits it would look like this:
        /*if (socialSecurityNumber.length() != 11){
            throw new IllegalArgumentException("Invalid socical security number." +
                    " Must be 11 digits");
        }*/
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     *
     * @return Returns first and last name as fullname.
     */
    public String getFullName(){
        return firstName + " " + lastName;
    }
    public abstract String toString();
}
