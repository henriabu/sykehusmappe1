package no.ntnu.henriabu.mappe.del1.hospital;

import no.ntnu.henriabu.mappe.del1.hospital.healthpersonal.doctor.GeneralPractitioner;
import no.ntnu.henriabu.mappe.del1.hospital.healthpersonal.doctor.Surgeon;

public class Employee extends Person {
    public Employee(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }
    /**
     *
     * @return
     * Returns toString for the different kind of Employees. If the employee is not an instance of Surgeon or
     * GeneralPractitioner they will be callen "Employee". Could be an accountant, cleaner and all other hospital
     * employees.
     * Does not return social security number since the toString will be used in the departments
     * toString and the SSN is sensitive information.
     */
    public String toString(){
        if (this instanceof Surgeon){
            return "Surgeon. " + "Name: " + getFullName() + "\n";
        }
        else if (this instanceof GeneralPractitioner){
            return "General practitioner. " + "Name: " + getFullName() + "\n";
        }
        else{
            return "Employee. " + "Name: " + getFullName() + "\n";
        }
    }
}
