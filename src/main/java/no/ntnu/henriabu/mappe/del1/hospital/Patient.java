package no.ntnu.henriabu.mappe.del1.hospital;

public class Patient extends Person implements Diagnosable {
    private String diagnosis = "";


    protected Patient(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    protected String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis){
        this.diagnosis = diagnosis;
    }

    /**
     *
     * @return Does not return social security number since the toString will be used in the departments
     * toString, and the SSN is sensitive information.
     */
    public String toString(){
        return "Patient: " + getFullName() + ". Diagnosis: " + diagnosis + "\n";
    }
}
