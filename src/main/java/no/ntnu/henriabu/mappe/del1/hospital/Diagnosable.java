package no.ntnu.henriabu.mappe.del1.hospital;

public interface Diagnosable {
    void setDiagnosis(String diagnosis);

}

