package no.ntnu.henriabu.mappe.del1.hospital.exception;

import java.io.Serial;
import java.io.Serializable;

public class RemoveException extends Exception implements Serializable{

    @Serial
    private static final long serialVersionUID = 1L;

    public RemoveException(String message){

        super(message);
    }
}
