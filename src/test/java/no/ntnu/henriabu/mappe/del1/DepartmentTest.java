package no.ntnu.henriabu.mappe.del1;
import no.ntnu.henriabu.mappe.del1.hospital.Department;
import no.ntnu.henriabu.mappe.del1.hospital.Employee;
import no.ntnu.henriabu.mappe.del1.hospital.exception.RemoveException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;

public class DepartmentTest {
    @Nested
    class Testing{
        Department department = new Department("Test Departement");
        Employee employee = new Employee("Ola", "Nordmann", "123");

        @Test
        @DisplayName("Checks if employee is removed")
        public void RemoveExceptionShouldBeThrown() throws RemoveException { // Employee is not added and RemoceException should be thrown
            Assertions.assertThrows(RemoveException.class, () -> {
                department.remove(employee);
            });
        }

        @Test
        @DisplayName("Checks if employee is removed")
        public void EmployeeShouldBeRemoved(){ // Employee is added and no exception should therfore be thrown
            department.addEmployee(employee);
            Assertions.assertDoesNotThrow( () ->
                department.remove(employee));

        }
    }
}
